#!/bin/bash

echo "Please select option given below:

	1. Hardware information
	2. See Packet loss
	3. To check if package is installed
	4. Network routing info
	5. File system info
	6. Get list of ports with interfaces (TCP/UDP)
	7. Get list of established connections
	8. Get top Memory consuming processes
	9. Get top CPU consuming processes
	10. Create CPU/Memory load
Please enter option from above:"

read i

case $i in
	"1") echo "hardware info available through
		a. lshw
		b. dmidecode

		Command?"
	       read j
       		case $j in 
	 	"a") lshw ;;
		"b") dmidecode ;;
		esac

		;;
	"2") echo "Find method out of below : 
		a. ss 
		b. netstat
		"
		read met

		case $met in 
			"a") ss -s ;;
			"b") netstat -s ;;
		esac
		;;

#		echo "For ping loss please provide below required fields:
#		
#		Destinition IP:"
#	       read IP
#       	    echo "Count:"
#		read count
#    		ping -c$count $IP	;;
	"3") echo "Package name:"
		read pkg
		if [[ $(dpkg-query -l $pkg) ]] ; then
			echo $pkg is installed in your system
		else
			echo $pkg is not installed
		fi;
		;;
	"4") echo "Command for network routing info:
		a. route -n
		b. ip r"
		read routes
		case $routes in
			"a") route -n ;;
			"b") ip r ;;
			*) echo "Invalid option, either enter option a or option b";;
		esac
		;;
	"5") echo $'\n'"
		  -------------------------
		    Disk usage is as follow
		  ----------------------------"
		df -h
	     echo $'\n'"
	        -------------------------
	          partition table is as follow
	        ----------------------------"
		fdisk -l
	     echo $'\n'"
	        -------------------------
		filesystem detail is as follow
	        ----------------------------"
		lsblk
		echo $'\n'"
		     -----------------------"
		blkid /dev/sda6
		;;
	"6") echo $'\n'"
		  --------------------------
		    Command to get list of ports with interfaces

		    a. ss
		    b. netstat
		  --------------------------
		    "
		    read conn

		    case $conn in
			    "a") ss -a | grep -E "^tcp|^udp" ;;
			    "b") netstat -nap | grep -E "^tcp|^udp" ;;
		    esac
		    ;;
	"7") echo $'\n'"
		  -------------------------
		    Get list of established connections

		    a. ss
		    b. netstat
		  ------------------------
		  "
		    read conn

		    case $conn in
			    "a") ss -a | grep "ESTA" ;;
			    "b") netstat -nap | grep "ESTA" ;;
		    esac
		    ;;
	"8") ps aux --sort -rss ;;
	"9") ps aux --sort pcpu ;;
	"10") echo $'\n'"
		   ----------------------------
		     Create load on

		     a. CPU
		     b. Memory
		     c. hdd
		   ---------------------------"
	     read l
	     case $l in 
		     "a") timeout 20 stress --cpu 3 ;;
		     "b") timeout 20 stress --vm 2 --vm-bytes 2000M;;
		     "c") timeout 20 stress --hdd 2 ;;
	     esac
	     ;;

esac
